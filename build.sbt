import com.typesafe.sbt.packager.docker._
import org.clapper.sbt.editsource.EditSourcePlugin
import org.clapper.sbt.editsource.EditSourcePlugin.autoImport.{edit, _}
import sbt.Keys.{javaOptions, libraryDependencies, run, scalacOptions}
import sbt.file

val appName = "xivo-switchboard-reports"
val appVersion = sys.env.getOrElse("TARGET_VERSION", "dev-version")
val appOrganisation = "xivo"

lazy val root = (project in file("."))
  .enablePlugins(DockerPlugin)
  .enablePlugins(JavaServerAppPackaging)
  .settings(
    name := appName,
    version := appVersion,
    scalaVersion := Dependencies.scalaVersion,
    resolvers ++= Dependencies.resolutionRepos,
    libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation"
    ),
  )
  .settings(setVersionSettings: _*)
  .settings(editSourceSettings: _*)
  .settings(compileSettings: _*)
  .settings(testSettings: _*)
  .settings(dockerSettings: _*)
  .settings(docSettings: _*)


lazy val setVersionVarTask = taskKey[Unit]("Set version to a env var")

lazy val setVersionSettings = Seq(
  setVersionVarTask := { System.setProperty("APPLI_VERSION", appVersion) },
  edit in EditSource := ((edit in EditSource) dependsOn (EditSourcePlugin.autoImport.clean in EditSource)).value
)

AssemblyPlugin.assemblySettings

lazy val editSourceSettings = Seq(
  flatten in EditSource := true,
  mappings in Universal += file("conf/appli.version") -> "conf/appli.version",
  targetDirectory in EditSource := baseDirectory.value / "conf",
  variables in EditSource += ("SBT_EDIT_APP_VERSION", appVersion),
  (sources in EditSource) ++= (baseDirectory.value / "src/res" * "appli.version").get,
  edit in EditSource := ((edit in EditSource) dependsOn (EditSourcePlugin.autoImport.clean in EditSource)).value
)

lazy val compileSettings = Seq(
  packageBin in Compile := ((packageBin in Compile) dependsOn (edit in EditSource)).value,
  run in Compile := ((run in Compile) dependsOn setVersionVarTask).evaluated
)

lazy val testSettings = Seq(
  parallelExecution in Test := false,
  javaOptions in Test += "-Dlogger.file=test/resources/logback-test.xml",
  testOptions in Test += Tests.Argument("-oD")
)

lazy val dockerSettings = Seq(
  maintainer in Docker := "R&D <randd@xivo.solutions>",
  dockerBaseImage := "openjdk:8u275-jdk-slim-buster",
  dockerExposedVolumes := Seq("/conf","/logs"),
  dockerExposedPorts ++= Seq(9510),
  dockerRepository := Some("xivoxc"),
  dockerCommands += Cmd("LABEL", s"""version="$appVersion""""),
  dockerCommands ++= Seq(
    Cmd("USER", "root"),
    Cmd("RUN", "set -eux"),
    Cmd("RUN", "apt-get update -y && apt-get install -y --no-install-recommends fontconfig libfreetype6"),
    Cmd("USER", "daemon")
  ),
  dockerEntrypoint := Seq("bin/xivo-switchboard-reports-docker"),
  daemonUserUid in Docker := None,
  daemonUser in Docker := "daemon",
  dockerChmodType := DockerChmodType.UserGroupWriteExecute,
)

lazy val docSettings = Seq(
  publishArtifact in(Compile, packageDoc) := false,
  sources in(Compile, doc) := Seq.empty
)
