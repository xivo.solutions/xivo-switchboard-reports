import sbt._

object Version {
  lazy val akkaHttpVersion = "10.2.4"
  lazy val akkaVersion     = "2.6.14"
  lazy val scalaVersion    = "2.13.5"
}

object Library {
  val akkaHttp          = "com.typesafe.akka"    %% "akka-http"            % Version.akkaHttpVersion
  val akkaStream        = "com.typesafe.akka"    %% "akka-stream"          % Version.akkaVersion
  val logback           = "ch.qos.logback"       %  "logback-classic"      % "1.2.3"
  val jasperReports     = "net.sf.jasperreports" %  "jasperreports"        % "6.16.0"
  val itext             = "com.lowagie"          %  "itext"                % "2.1.7"
  val postgresql        = "org.postgresql"       %  "postgresql"           % "42.2.20"
  val hikaricp          = "com.zaxxer"           %  "HikariCP"             % "4.0.3"


  val akkaHttpTestkit   = "com.typesafe.akka"    %% "akka-http-testkit"    % Version.akkaHttpVersion
  val akkaStreamTestkit = "com.typesafe.akka"    %% "akka-stream-testkit"  % Version.akkaVersion
  val scalatest         = "org.scalatest"        %% "scalatest"            % "3.0.8"
  val mockito           = "org.mockito"          %% "mockito-scala"        % "1.11.2"
}

object Dependencies {

  import Library._

  val scalaVersion: String = Version.scalaVersion

  val resolutionRepos = Seq(
    "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/",
    "Java.net Maven2 Repository" at "https://download.java.net/maven/2/"
  )

  val runDep = run(
    akkaHttp,
    akkaStream,
    logback,
    jasperReports,
    itext,
    postgresql,
    hikaricp
  )

  val testDep = test(
    akkaHttpTestkit,
    akkaStreamTestkit,
    scalatest,
    mockito
  )

  def run(deps: ModuleID*): Seq[ModuleID] = deps
  def test(deps: ModuleID*): Seq[ModuleID] = deps map (_ % Test)
}
