# xivo-switchboard-reports

PDF export of the switchboard queues statistics via the XiVO Webi


# Development

- Start XiVO and CC (stats database)
- Edit `object/switchboard/config.inc` (use `docker exec -u 0 -ti xivo_webi_vol /bin/bash`, you may need to `apt update && apt install vim`)
  - Change `host` to your PC
- Edit `application.conf`:
  - Change `reporting.address` to stats db host
  - Change `reporting.switchboardReport` to `src/universal/reports/switchboard.jrxml`
- Create directory `/var/log/xivo-switchboard-reports/` owned by you
- Run `sbt`, then `run`
- Click on download button in webi

# Test docker package

- Check that you reverted webi `object/switchboard/config.inc` file to default value.
- launch `./docker_build.sh`
- get docker <image_id> (from `docker images` command)
- then send it to  your XiVO VM trough with following command `docker save <image_id> | bzip2 | pv | ssh root@xivo 'bunzip2 | docker load'`
- xivo-dcomp up -d
- Click on download button in webi
