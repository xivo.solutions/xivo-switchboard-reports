package reporting

import java.io.{BufferedInputStream, FileInputStream}
import java.sql.Connection

import net.sf.jasperreports.engine._
import net.sf.jasperreports.engine.design.JasperDesign
import net.sf.jasperreports.engine.xml.JRXmlLoader
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Try, Using}
import com.zaxxer.hikari.HikariDataSource

class JasperReportingExecutor(config: Configuration)(implicit val ec: ExecutionContext) {

  val logger: Logger = LoggerFactory.getLogger(getClass)
  val jasperReportFile: String = config.getReportingTemplate

  def createReport(params: Map[String, String]): Future[Array[Byte]] = {
    val futureReport: Future[Array[Byte]] =
      getConnection.flatMap { conn =>
        Future.fromTry(loadJasperDesign(jasperReportFile))
          .map(compileJasperReport)
          .map(r => printJasperReport(r, params, conn))
          .map(exportJasperPrint)
          .andThen(_ => conn.close())
      }
    futureReport
  }

  private def getConnection: Future[Connection] = {
    config.getConnection
  }

  private def loadJasperDesign(fileName: String): Try[JasperDesign] = {
    Using(new BufferedInputStream(new FileInputStream(fileName))) { reader =>
      val design: JasperDesign = JRXmlLoader.load(reader)
      design
    }
  }

  private def compileJasperReport(design: JasperDesign): JasperReport = {
    JasperCompileManager.compileReport(design)
  }

  private def printJasperReport(report: JasperReport, params: scala.collection.Map[String, AnyRef], connection: Connection): JasperPrint = {
    val parameters = new java.util.HashMap[String, AnyRef]()
    params.map { case (k, v) => parameters.put(k, v) }
    JasperFillManager.fillReport(report, parameters, connection)
  }

  private def exportJasperPrint(print: JasperPrint): Array[Byte] = {
    JasperExportManager.exportReportToPdf(print)
  }
}
