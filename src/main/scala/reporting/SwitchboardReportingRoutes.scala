package reporting

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{HttpEntity, HttpResponse, MediaTypes, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.util.ByteString
import org.slf4j.{Logger, LoggerFactory}

import scala.util.{Failure, Success}

import scala.concurrent.duration._

class SwitchboardReportingRoutes(jasperReporting: JasperReportingExecutor)(implicit val system: ActorSystem) {
  val logger: Logger = LoggerFactory.getLogger(getClass)
  val routeTimeout = 180.seconds

  val switchboardRoutes: Route =
    path("report") {
      post {
          withRequestTimeout(routeTimeout, _ => createHttpTimeoutResponse) {
            formFields(Symbol("start_date"), Symbol("end_date")) { (start, end) =>
              logger.info(s"Requesting switchboard report from: $start to: $end")
              onComplete(jasperReporting.createReport(toReportParams(start, end))) {
                case Success(value) => complete(createHttpResponse(value))
                case Failure(f) =>
                  logger.error(s"Unable to create report. ${f.getMessage}")
                  complete(StatusCodes.InternalServerError, s"Unable to create report. ${f.getMessage}")
              }
            }
          }
      }
    }

  private def toReportParams(start: String, end: String): Map[String, String] = {
    Map("date_debut" -> start, "date_fin" -> end)
  }

  private def createHttpResponse(content: Array[Byte]): HttpResponse = {
    HttpResponse(entity = HttpEntity.Strict(MediaTypes.`application/pdf`, ByteString(content)))
  }

  private def createHttpTimeoutResponse: HttpResponse = {
    logger.error(s"Unable to serve response within time limit. Try to narrow the report period.")
    HttpResponse(StatusCodes.RequestTimeout)
  }
}
