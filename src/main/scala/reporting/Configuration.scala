package reporting

import java.sql.Connection

import akka.actor.ActorSystem
import com.typesafe.config.{Config, ConfigFactory}
import com.zaxxer.hikari.HikariDataSource
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration.{FiniteDuration, _}
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.language.implicitConversions

class Configuration(implicit val system: ActorSystem, ec: ExecutionContext) {
  implicit def asFiniteDuration(d: java.time.Duration): FiniteDuration =
    scala.concurrent.duration.Duration.fromNanos(d.toNanos)

  val DB_CNX_TIMEOUT: FiniteDuration = 60.seconds
  val logger: Logger = LoggerFactory.getLogger(getClass)
  private val appConf: Config = ConfigFactory.load()

  private val connectionFactory: ConnectionFactory = new ConnectionFactory(
      appConf.getString("reporting.address"),
      appConf.getInt("reporting.port"),
      appConf.getString("reporting.database.name"),
      appConf.getString("reporting.database.user"),
      appConf.getString("reporting.database.password"))

  private final val databaseConnectionPool: Future[HikariDataSource] = initConnection

  private def initConnection: Future[HikariDataSource] = {

    val futureConnection = akka.pattern.Patterns.retry[HikariDataSource](
      () => tryGetConnection(connectionFactory),
      attempts = 60,
      delay = 1.second,
      scheduler = system.scheduler,
      context = ec
    )
    futureConnection.failed.foreach { _ =>
      logger.error("Unable to connect to database during $DB_CNX_TIMEOUT, exiting the application")
      system.terminate()
    }
    futureConnection
  }

  def getConnection: Future[Connection] = {
    databaseConnectionPool.map(_.getConnection)
  }

  def closeConnectionPool(): Unit = {
    databaseConnectionPool.map(_.close())
  }

  private def tryGetConnection(factory: ConnectionFactory): Future[HikariDataSource] = {
    try {
      logger.info("Initializing connection to the database")
      Future.successful(factory.initDataSource)
    } catch {
      case e: Exception =>
        logger.error("Unable to get connection")
        Future.failed(e)
    }
  }

  def getReportingTemplate: String = {
    appConf.getString("reporting.switchboardReport")
  }
}
