package reporting

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

class HttpServer(implicit val system: ActorSystem, ec: ExecutionContext) {
  val logger: Logger = LoggerFactory.getLogger(getClass)
  val interface = "0.0.0.0"
  val port = 9510

  def startHttpServer(routes: Route): Unit = {
    val futureBinding = Http().newServerAt(interface, port).bind(routes)
    futureBinding.onComplete {
      case Success(binding) =>
        val address = binding.localAddress
        logger.info("Server accepting requests at http://{}:{}/", address.getHostString, address.getPort)
      case Failure(ex) =>
        logger.error("Failed to bind HTTP endpoint, terminating system", ex)
        system.terminate()
    }
  }
}

