package reporting

import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import org.slf4j.{Logger, LoggerFactory}

class ConnectionFactory(host: String, port:Int, dbName: String, dbUser: String, dbPassword: String) {
  val logger: Logger = LoggerFactory.getLogger(getClass)
  logger.info("----- Database configuration -------")
  logger.info(s"host    : $host:$port")
  logger.info(s"db name : $dbName")
  logger.info(s"db user : $dbUser")

  def initDataSource: HikariDataSource = {
    logger.info("Initializing the database connection")

    val uri = "jdbc:postgresql://" + host + ":" + port + "/" + dbName
    val hikariConfig = new HikariConfig()

    hikariConfig.setJdbcUrl(uri)
    hikariConfig.setUsername(dbUser)
    hikariConfig.setPassword(dbPassword)
    hikariConfig.setConnectionTimeout(2000)
    hikariConfig.setMaximumPoolSize(2)

    new HikariDataSource(hikariConfig)
  }
}
