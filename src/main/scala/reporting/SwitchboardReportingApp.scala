package reporting

import akka.actor.ActorSystem
import akka.util.Timeout
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.Await
import scala.concurrent.duration._

object SwitchboardReportingApp {
  val logger: Logger = LoggerFactory.getLogger(getClass)
  val timeout: Timeout = Timeout(10.seconds)

  def main(args: Array[String]): Unit = {
    implicit val system: ActorSystem = ActorSystem("SwitchboardReporting")
    implicit val executionContext: ExecutionContextExecutor = system.dispatcher

    val configuration = new Configuration

    val httpServer = new HttpServer
    val jasperReporting = new JasperReportingExecutor(configuration)
    val routes = new SwitchboardReportingRoutes(jasperReporting)

    httpServer.startHttpServer(routes.switchboardRoutes)

    scala.sys.addShutdownHook {
      logger.info("Stopping...")
      configuration.closeConnectionPool()
      logger.info("Stopped...")
    }
  }
}
