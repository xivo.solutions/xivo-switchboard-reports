package reporting

import akka.actor.ActorSystem
import akka.http.scaladsl.model.MediaTypes._
import akka.http.scaladsl.model.headers.{RawHeader, `Content-Type`}
import akka.http.scaladsl.model.{FormData, HttpRequest, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.{RouteTest, RouteTestTimeout, ScalatestRouteTest}
import org.mockito.Mockito.when
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{Matchers, WordSpec}
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.Future
import scala.concurrent.duration.{DurationInt, FiniteDuration}

class SwitchboardReportingRoutesSpec extends WordSpec with Matchers with MockitoSugar with ScalaFutures with ScalatestRouteTest {

  class Helper() {
    val jasperReporting: JasperReportingExecutor = mock[JasperReportingExecutor]
    val routes = new SwitchboardReportingRoutes(jasperReporting) {
      override val routeTimeout: FiniteDuration = 1000.milliseconds
    }

    val formFields: FormData = FormData("start_date" -> "2020-01-01", "end_date" -> "2020-02-29")
    val formFieldsMap = Map("date_debut" -> "2020-01-01", "date_fin" -> "2020-02-29")
  }

  "SwitchboardReportingRoutesSpec" should {
    "return pdf file" in new Helper {
      val dummyContent: Array[Byte] = Array.fill(20)(1.toByte)
      val request: HttpRequest = Post("/report", formFields)

      when(jasperReporting.createReport(formFieldsMap))
        .thenReturn(Future.successful(dummyContent))

      request ~> routes.switchboardRoutes ~> check {
        status shouldEqual StatusCodes.OK
        header[`Content-Type`] shouldEqual Some(`Content-Type`(`application/pdf`))
        responseAs[Array[Byte]] shouldEqual dummyContent
      }
    }

    "return bad request if form fields are missing" in new Helper {
      val dummyContent: Array[Byte] = Array.fill(20)(1.toByte)
      val request: HttpRequest = Post("/report")

      request ~> Route.seal(routes.switchboardRoutes) ~> check {
        status shouldEqual StatusCodes.BadRequest
        responseAs[String] shouldEqual "Request is missing required form field 'start_date'"
      }
    }

    "return method not allowed if not a POST request" in new Helper {
      val request: HttpRequest = Get("/report")

      request ~> Route.seal(routes.switchboardRoutes) ~> check {
        status shouldEqual StatusCodes.MethodNotAllowed
        responseAs[String] shouldEqual "HTTP method not allowed, supported methods: POST"
      }
    }

    "return error if report creating fails" in new Helper {

      val dummyContent: Array[Byte] = Array.fill(20)(1.toByte)
      val request: HttpRequest = Post("/report", formFields)

      when(jasperReporting.createReport(formFieldsMap))
        .thenReturn(Future.failed(new Exception("Failure.")))

      request ~> routes.switchboardRoutes ~> check {
        status shouldEqual StatusCodes.InternalServerError
        responseAs[String] shouldEqual "Unable to create report. Failure."
      }
    }

    /**
     * Commented due to full route integration testing with timeout.
     * Will be fixed in akka-http-testkit 10.2.0, currently 10.2.0-M1
     * See: https://github.com/akka/akka-http/issues/952
     */
//    "return timeout" in new Helper {
//      implicit def default(implicit system: ActorSystem): RouteTestTimeout = RouteTestTimeout(new DurationInt(5).second)
//      val dummyContent: Future[Array[Byte]] = Future{
//        Thread.sleep(2000)
//        Array.fill(20)(1.toByte)
//      }
//
//      val timeoutHeader = Seq(RawHeader("`Timeout-Access`", "1000"))
//
//      val request: HttpRequest = Post("/report", formFields)
//          .withHeaders(timeoutHeader)
//
//      when(jasperReporting.createReport(formFieldsMap))
//        .thenReturn(dummyContent)
//
//      request ~!> {
//        routes.switchboardRoutes
//      } ~> check {
//        status shouldEqual StatusCodes.RequestTimeout
//      }
//    }
  }
}
